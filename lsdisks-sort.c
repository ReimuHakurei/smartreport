#define _XOPEN_SOURCE 700

#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <sys/dir.h>
#include <unistd.h>

int prefix(const char *pre, const char *str) {
	return strncmp(pre, str, strlen(pre)) == 0;
}

int main() {
	struct dirent **dp;
	char * file_name;

	int n;
	int i = 0;

	n = scandir("/dev/disk/by-id/",&dp,NULL,alphasort);
	while (i<n) {
		file_name = dp[i++]->d_name;

		if ( strcmp(file_name, ".") || strcmp(file_name, "..") ) {
			// Get path that by-id disk is a symlink to, then use this as the ID to reutrn.
			// By using by-id for the sorting, we can sort disks by model number, for a
			// cleaner readout.

			if (strstr(file_name,"-part") == NULL && prefix("ata",file_name)) {
				// Items in /dev/disk/by-id/ are symlinks to normal devices (ie: sda, etc).
				// We find out the target, and report that to the calling process.

				char fullpath[512];

				sprintf(fullpath,"/dev/disk/by-id/%s",file_name);

				char linkname[512];
				memset(linkname,0,512);

				readlink(fullpath, linkname, 512);

				for (int k = 0; k < sizeof(linkname); k++) {
					linkname[k]=linkname[k+6];
				}

				printf("%s ",linkname);
			}
		}
	}

	return 0;
}
