SMART Report
====================

Introduction
---------------------
SMART Report is a shell script and C program to automatically scan the SMART stats of all of the disks connected to 
a Linux system, and print an easily-readable table with the core SMART stats. 

Based on the [drivereport script](https://github.com/lukeroge/freenas_scripts) by lukeroge on GitHub.

Installation
---------------------
`root@linux:~/smartreport# chmod +x install`

`root@linux:~/smartreport# ./install`

Usage
---------------------
`root@linux:~# smartreport`
