#define _XOPEN_SOURCE 700

#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <sys/dir.h>

int prefix(const char *pre, const char *str) {
	return strncmp(pre, str, strlen(pre)) == 0;
}

int main() {
	struct dirent **dp;
	char * file_name;

	int n;
	int i = 0;

	n = scandir("/dev/",&dp,NULL,alphasort);
	while (i<n) {
		file_name = dp[i++]->d_name;

		if ( strcmp(file_name, ".") || strcmp(file_name, "..") ) {

			char lastchar = file_name[strlen(file_name) - 1];

			if (prefix("sd",file_name)) {
                                if (lastchar != '0' && lastchar != '1' && lastchar != '2' && lastchar != '3' && lastchar != '4' \
				 && lastchar != '5' && lastchar != '6' && lastchar != '7' && lastchar != '8' && lastchar != '9') {
					printf("%s ",file_name);
				}
			}
		}
	}

	return 0;
}
